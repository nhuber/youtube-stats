from __future__ import annotations

from functools import reduce

import requests
import isodate

from django.db import models
from django.conf import settings


class Channel(models.Model):
    title = models.CharField(max_length=128)
    description = models.TextField()
    thumbnail_url = models.CharField(max_length=128)
    username = models.CharField(max_length=128, null=True)
    channel_id = models.CharField(max_length=128)
    num_subscribers = models.IntegerField()
    upload_playlist_id = models.CharField(max_length=128)

    def __str__(self):
        return self.title

    @classmethod
    def create_from_username(cls, username: str) -> Channel:
        statistics_response = requests.get(
            "https://www.googleapis.com/youtube/v3/channels",
            params={
                "part": "statistics",
                "forUsername": username,
                "key": settings.YOUTUBE_API_KEY,
            },
        )
        stats = statistics_response.json()["items"][0]["statistics"]
        channel_id = statistics_response.json()["items"][0]["id"]
        details_response = requests.get(
            "https://www.googleapis.com/youtube/v3/channels",
            params={
                "part": "contentDetails",
                "forUsername": username,
                "key": settings.YOUTUBE_API_KEY,
            },
        )
        details = details_response.json()["items"][0]["contentDetails"]
        playlist_id = details["relatedPlaylists"]["uploads"]
        snippet_response = requests.get(
            "https://www.googleapis.com/youtube/v3/channels",
            params={
                "part": "snippet",
                "forUsername": username,
                "key": settings.YOUTUBE_API_KEY,
            },
        )
        snippet = snippet_response.json()["items"][0]["snippet"]
        if "high" in snippet["thumbnails"]:
            thumbnail_url = snippet["thumbnails"]["high"]["url"]
        elif "medium" in snippet["thumbnails"]:
            thumbnail_url = snippet["thumbnails"]["medium"]["url"]
        else:
            thumbnail_url = snippet["thumbnails"]["default"]["url"]
        return cls.objects.create(
            title=snippet["title"],
            description=snippet["description"],
            thumbnail_url=thumbnail_url,
            username=username,
            upload_playlist_id=playlist_id,
            channel_id=channel_id,
            num_subscribers=stats["subscriberCount"],
        )

    @classmethod
    def create_from_channel_id(cls, channel_id: str) -> Channel:
        statistics_response = requests.get(
            "https://www.googleapis.com/youtube/v3/channels",
            params={
                "part": "statistics",
                "id": channel_id,
                "key": settings.YOUTUBE_API_KEY,
            },
        )
        stats = statistics_response.json()["items"][0]["statistics"]
        details_response = requests.get(
            "https://www.googleapis.com/youtube/v3/channels",
            params={
                "part": "contentDetails",
                "id": channel_id,
                "key": settings.YOUTUBE_API_KEY,
            },
        )
        details = details_response.json()["items"][0]["contentDetails"]
        playlist_id = details["relatedPlaylists"]["uploads"]
        snippet_response = requests.get(
            "https://www.googleapis.com/youtube/v3/channels",
            params={
                "part": "snippet",
                "id": channel_id,
                "key": settings.YOUTUBE_API_KEY,
            },
        )
        snippet = snippet_response.json()["items"][0]["snippet"]
        if "high" in snippet["thumbnails"]:
            thumbnail_url = snippet["thumbnails"]["high"]["url"]
        elif "medium" in snippet["thumbnails"]:
            thumbnail_url = snippet["thumbnails"]["medium"]["url"]
        else:
            thumbnail_url = snippet["thumbnails"]["default"]["url"]
        return cls.objects.create(
            title=snippet["title"],
            description=snippet["description"],
            thumbnail_url=thumbnail_url,
            upload_playlist_id=playlist_id,
            channel_id=channel_id,
            num_subscribers=stats["subscriberCount"],
        )

    def update_stats(self):
        statistics_response = requests.get(
            "https://www.googleapis.com/youtube/v3/channels",
            params={
                "part": "statistics",
                "id": self.channel_id,
                "key": settings.YOUTUBE_API_KEY,
            },
        )
        stats = statistics_response.json()["items"][0]["statistics"]
        snippet_response = requests.get(
            "https://www.googleapis.com/youtube/v3/channels",
            params={
                "part": "snippet",
                "id": self.channel_id,
                "key": settings.YOUTUBE_API_KEY,
            },
        )
        snippet = snippet_response.json()["items"][0]["snippet"]
        if "high" in snippet["thumbnails"]:
            thumbnail_url = snippet["thumbnails"]["high"]["url"]
        elif "medium" in snippet["thumbnails"]:
            thumbnail_url = snippet["thumbnails"]["medium"]["url"]
        else:
            thumbnail_url = snippet["thumbnails"]["default"]["url"]
        self.title = snippet["title"]
        self.description = snippet["description"]
        self.thumbnail_url = thumbnail_url
        self.num_subscribers = stats["subscriberCount"]
        self.save()

    def create_new_videos(self):
        next_page_token = ""
        while next_page_token is not None:
            playlist_response = requests.get(
                "https://www.googleapis.com/youtube/v3/playlistItems",
                params={
                    "part": "snippet",
                    "maxResults": settings.VIDEOS_PER_REQUEST,
                    "playlistId": self.upload_playlist_id,
                    "pageToken": next_page_token,
                    "key": settings.YOUTUBE_API_KEY,
                },
            )
            next_page_token = playlist_response.json().get("nextPageToken")
            for item in playlist_response.json()["items"]:
                video_id = item["snippet"]["resourceId"]["videoId"]
                if self.video_set.filter(video_id=video_id).exists():
                    return
                Video.create_from_video_id(self, video_id)

    def get_video_publish_stats(self):
        last_published = None
        differences = []
        for video in self.video_set.all().order_by("publish_time"):
            if last_published:
                differences.append(video.publish_time - last_published)
            last_published = video.publish_time
        if len(differences) == 0:
            return None
        average_time = reduce(lambda x, y: x + y, differences) / len(differences)
        next_video_date = (
            self.video_set.all().order_by("-publish_time").first().publish_time
            + average_time
        )
        return {
            "avg": average_time,
            "longest_drought": max(differences),
            "next_video_date": next_video_date,
        }


class Video(models.Model):
    channel = models.ForeignKey(Channel, on_delete=models.CASCADE)
    video_id = models.CharField(max_length=128)
    title = models.CharField(max_length=128)
    description = models.TextField()
    duration_seconds = models.DurationField()
    publish_time = models.DateTimeField()
    num_views = models.IntegerField()
    num_likes = models.IntegerField(null=True)
    num_dislikes = models.IntegerField(null=True)
    num_favourites = models.IntegerField()
    num_comments = models.IntegerField(null=True)

    def __str__(self):
        return self.title

    @classmethod
    def create_from_video_id(cls, channel: Channel, video_id: str) -> Video:
        snippet_response = requests.get(
            "https://www.googleapis.com/youtube/v3/videos",
            params={"part": "snippet", "id": video_id, "key": settings.YOUTUBE_API_KEY},
        )
        snippet = snippet_response.json()["items"][0]["snippet"]
        statistics_response = requests.get(
            "https://www.googleapis.com/youtube/v3/videos",
            params={
                "part": "statistics",
                "id": video_id,
                "key": settings.YOUTUBE_API_KEY,
            },
        )
        stats = statistics_response.json()["items"][0]["statistics"]
        details_respoonse = requests.get(
            "https://www.googleapis.com/youtube/v3/videos",
            params={
                "part": "contentDetails",
                "id": video_id,
                "key": settings.YOUTUBE_API_KEY,
            },
        )
        details = details_respoonse.json()["items"][0]["contentDetails"]
        publish_time = isodate.isodatetime.parse_datetime(snippet["publishedAt"])
        return cls.objects.create(
            channel=channel,
            video_id=video_id,
            title=snippet["title"],
            description=snippet["description"],
            publish_time=publish_time,
            duration_seconds=isodate.parse_duration(details["duration"]),
            num_views=stats["viewCount"],
            num_likes=stats.get("likeCount"),
            num_dislikes=stats.get("dislikeCount"),
            num_favourites=stats["favoriteCount"],
            num_comments=stats.get("commentCount"),
        )

    def update_stats(self):
        statistics_response = requests.get(
            "https://www.googleapis.com/youtube/v3/videos",
            params={
                "part": "statistics",
                "id": self.video_id,
                "key": settings.YOUTUBE_API_KEY,
            },
        )
        stats = statistics_response.json()["items"][0]["statistics"]
        self.num_views = stats["viewCount"]
        self.num_likes = stats.get("likeCount")
        self.num_dislikes = stats.get("dislikeCount")
        self.num_favourites = stats["favoriteCount"]
        self.num_comments = stats.get("commentCount")
        self.save()
