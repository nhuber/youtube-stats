from django.core.management.base import BaseCommand, CommandError
from stats.models import Channel


class Command(BaseCommand):
    help = "Update channels with new videos, and videos with latest statistics"

    def handle(self, *args, **options):
        for channel in Channel.objects.all():
            self.stdout.write(f"Updating channel {channel.title} information")
            channel.update_stats()
            self.stdout.write(f"Getting new videos for {channel.title}")
            channel.create_new_videos()
            for video in channel.video_set.all():
                self.stdout.write(f"Updating video statistics for {video.title}")
                video.update_stats()
