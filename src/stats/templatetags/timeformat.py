from datetime import timedelta
from django import template

register = template.Library()


@register.filter
def timesince(value: timedelta) -> str:
    """Removes all values of arg from the given string"""
    if value.days and value.seconds >= 3600:
        return f"{value.days} days and {value.seconds // 3600} hours"
    elif value.days:
        return f"{value.days} days"
    elif value.seconds >= 3600:
        return f"{value.seconds // 3600} hours"
