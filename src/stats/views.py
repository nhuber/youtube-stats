import datetime

import pygal

from django.db.models import Count, Sum
from django.shortcuts import render
from django.utils import timezone

from stats.models import Channel


def channel_comparison(request):
    num_videos = pygal.Bar(
        height=400, width=325, explicit_size=True, legend_at_bottom=True, js=[]
    )
    num_videos.title = "Number of videos"
    num_views = pygal.Bar(
        height=400, width=325, explicit_size=True, legend_at_bottom=True, js=[]
    )
    num_views.title = "Number of views"
    num_likes = pygal.Bar(
        height=400, width=325, explicit_size=True, legend_at_bottom=True, js=[]
    )
    num_likes.title = "Number of likes"
    num_comments = pygal.Bar(
        height=400, width=325, explicit_size=True, legend_at_bottom=True, js=[]
    )
    num_comments.title = "Number of comments"
    channels = Channel.objects.all().annotate(
        num_videos=Count("video"),
        num_views=Sum("video__num_views"),
        num_likes=Sum("video__num_likes"),
        num_comments=Sum("video__num_comments"),
    )
    oldest_allowed = timezone.now() - datetime.timedelta(days=365)
    published_videos = pygal.DateTimeLine(
        stroke=False,
        height=150,
        show_legend=False,
        x_label_rotation=45,
        x_value_formatter=lambda dt: dt.strftime('%d %b %Y'),
        show_y_guides=False,
        js=[]
    )
    published_videos.y_labels = []
    for idx, channel in enumerate(channels):
        published_videos.y_labels.append({'label': channel.title, 'value': idx})
        published_videos.add(
            channel.title,
            [{
                'value': (video.publish_time, idx),
                'label': video.title,
                'formatter': lambda s: f"{datetime.datetime.fromtimestamp(s[0]).strftime('%d %b %Y')}",
                'xlink': f'https://youtube.com/watch?v={video.video_id}',
            } for video in channel.video_set.filter(publish_time__gt=oldest_allowed)],
        )
        num_videos.add(channel.title, channel.num_videos)
        num_views.add(channel.title, channel.num_views)
        num_likes.add(channel.title, channel.num_likes)
        num_comments.add(channel.title, channel.num_comments)

    return render(
        request,
        "stats/channel_comparison.html",
        {
            "channels": list(
                (
                    channel,
                    channel.video_set.all().count(),
                    channel.video_set.order_by("publish_time").last().publish_time,
                    channel.get_video_publish_stats(),
                )
                for channel in channels
            ),
            "charts": {
                "amount_of_videos": num_videos.render(is_unicode=True),
                "amount_of_views": num_views.render(is_unicode=True),
                "amount_of_likes": num_likes.render(is_unicode=True),
                "amount_of_comments": num_comments.render(is_unicode=True),
                "published_videos": published_videos.render(is_unicode=True),
            },
        },
    )
