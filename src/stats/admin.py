from django.contrib import admin
from django.shortcuts import render, redirect
from django.urls import path
from django import forms

from stats.models import Channel, Video


class ChannelUsernameForm(forms.Form):
    username = forms.CharField()


class ChannelIDForm(forms.Form):
    channel_id = forms.CharField()


def fetch_videos(modeladmin, request, queryset):
    for q in queryset:
        q.create_new_videos()


fetch_videos.short_description = "Fetch new videos"


def update_stats(modeladmin, request, queryset):
    for q in queryset:
        q.update_stats()


update_stats.short_description = "Update channel stats"


@admin.register(Channel)
class ChannelAdmin(admin.ModelAdmin):
    change_list_template = "stats/admin/custom_list.html"
    list_display = ("title", "username", "channel_id", "num_subscribers")
    actions = [fetch_videos, update_stats]

    def get_urls(self):
        return [
            path("add-by-username/", self.add_by_username),
            path("add-by-channel-id/", self.add_by_channel_id),
        ] + super().get_urls()

    def add_by_username(self, request):
        if request.method == "POST":
            username = request.POST["username"]
            Channel.create_from_username(username)
            return redirect("..")
        form = ChannelUsernameForm()
        return render(request, "stats/admin/create_form.html", {"form": form})

    def add_by_channel_id(self, request):
        if request.method == "POST":
            channel_id = request.POST["channel_id"]
            Channel.create_from_channel_id(channel_id)
            return redirect("..")
        form = ChannelIDForm()
        return render(request, "stats/admin/create_form.html", {"form": form})


@admin.register(Video)
class VideoAdmin(admin.ModelAdmin):
    list_display = (
        "channel",
        "title",
        "publish_time",
        "num_views",
        "num_likes",
        "num_dislikes",
        "num_favourites",
        "num_comments",
    )
