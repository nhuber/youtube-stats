from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path
import stats.views

urlpatterns = [
    path("admin/", admin.site.urls),
    path("", stats.views.channel_comparison),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
