#!/bin/bash

set -x

dnf install sudo rsync

useradd youtube-stats

mkdir --parent /opt/youtube/app
rsync -a --delete src/ /opt/youtube/app/

cp gunicorn_config.py /opt/youtube/gunicorn_config.py

chown -R youtube-stats:youtube-stats /opt/youtube

if [ ! -d /opt/youtube/venv ] ; then
    python3 -m venv /opt/youtube/venv
fi

if [ ! -f /opt/youtube/django-secret-key ] ; then
    tr -dc 'a-z0-9!@#$%^&*(-_=+)' < /dev/urandom | head -c50 > /opt/youtube/django-secret-key
fi

if [ ! -f /opt/youtube/youtube-api-key ] ; then
    read -p "Enter your Youtube API key: " youtube_key
    echo $youtube_key > /opt/youtube/youtube-api-key
fi

sudo -u youtube-stats /opt/youtube/venv/bin/pip install -r requirements.pip
sudo -u youtube-stats /opt/youtube/venv/bin/python /opt/youtube/app/manage.py migrate
sudo -u youtube-stats /opt/youtube/venv/bin/python /opt/youtube/app/manage.py collectstatic

install --mode 0644 youtube-stats-refresh.service /etc/systemd/system/
install --mode 0644 youtube-stats-refresh.timer /etc/systemd/system/
install --mode 0644 youtube-stats.service /etc/systemd/system/
install --mode 0644 youtube-stats.socket /etc/systemd/system/
